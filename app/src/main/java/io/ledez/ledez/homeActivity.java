package io.ledez.ledez;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import io.ledez.ledez.R;

public class homeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.home);
    }
}
